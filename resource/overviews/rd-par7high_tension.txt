﻿GAME
{
	"pos_x"		"-17000"
	"pos_y"		"10861"
	"scale"		"20.0"
	"material" 		"vgui/swarm/Overviews/rd_par_Mission3Overview"
	"briefingmaterial" 	"vgui/swarm/Overviews/rd_par_Mission3BriefingOverview"
	//"missiontitle"		"High Tension"
	"missiontitle"		"고조된 긴장 "
	//"description"		"Looks like they did genetical research in this place, so now that we know what happens there, we should inform the IAF Command Center, that is to say find a communication center."
	"description"		"여기서 그들이 유전적인 연구를 한 것 같아 보여, 거기에 무슨 일이 벌어질지 알게됐으니, 동맹군 지휘실에 알려야죠, 통신실을 찾아야 한다고 말입니다."
	"image"			"swarm/MissionPics/rd_par_mission3"

	"builtin" 		"1"
	"version"		"1"
	"author"		"Eclipse"
	"website"		""
}
