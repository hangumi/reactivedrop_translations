﻿GAME
{
	// identifier for this campaign
	"CampaignName" "데스 매치"
	
	// description shown on the main menu when choosing which campaign to play
	"CampaignDescription" "움직이는 모든 걸 사살하십시오."
	
	// texture used on the main menu when choosing which campaign to play
	"ChooseCampaignTexture" "campaignpic/Deathmatch_Icon"
	
	// texture used on the campaign screen for the map
	"CampaignTextureName" "campaignpic/Deathmatch_Icon"
	
	// these textures are overlaid on top of the campaign map in order
	"CampaignTextureLayer1" "swarm/Campaign/CampaignMap_EmptyLayer"			//"CampaignTextureLayer1" "swarm/Campaign/JacobCampaignMap_Haze"
	"CampaignTextureLayer2" "swarm/Campaign/CampaignMap_EmptyLayer"			//"CampaignTextureLayer2" "swarm/Campaign/JacobCampaignMap_SnowNear"
	"CampaignTextureLayer3" "swarm/Campaign/CampaignMap_EmptyLayer"			//"CampaignTextureLayer3" "swarm/Campaign/JacobCampaignMap_SnowFar"
	
	// custom campaign credits file
	"CustomCreditsFile" 	"resource/deathmatch_credits"

	// position of this campaign in the galactic map (coords go from 0 to 1023)
	"GalaxyX"   "660"
	"GalaxyY"   "262"
	
	// searchlights (optional, max of 4)
	// angle: 0 is right, 90 is up, 180 is left, 270 is down
	//"Searchlight1X" "217"
	//"Searchlight1Y" "860"
	//"Searchlight1Angle" "80"
	//"Searchlight2X" "263"
	//"Searchlight2Y" "751"
	//"Searchlight2Angle" "100"
	//"Searchlight3X" "92"
	//"Searchlight3Y" "446"
	//"Searchlight3Angle" "90"
	//"Searchlight4X" "580"
	//"Searchlight4Y" "266"
	//"Searchlight4Angle" "90"
	
	// first mission entry is a dummy for the starting point
	"MISSION"
	{
		"MissionName"		"Entry Port"
		"MapName"		"start_area"
		"LocationX"		"500"
		"LocationY"		"500"
		"DifficultyModifier" 	"-2"
		"Links"			"dm_desert" 
		"LocationDescription"  	"Start"
		"ShortBriefing"  	""
	}
	
	// each mission listed
	"MISSION"
	{
		"MissionName"		"사막 데스매치"		// name used on the map screen, etc.
		"MapName"		"dm_desert"		// name of the map file
		"LocationX"		"100"				// location of the dot on the map
		"LocationY"		"150"				// (from 0 to 1023, on the above texture)
		"ThreatString" 		"1"    				// Threat level string to help players decide where to go next
		"Links"			"start_area dm_deima"	// map names of neighbours
		"LocationDescription" 	"사막"
		"ShortBriefing"  	""
		"AlwaysVisible"		"1"
		"NeedsMoreThanOneMarine" "0"				// If set to 1, two players need to be connected to play (no solo play)
	}
	"MISSION"
	{
		"MissionName"	"데이마 표면교 데스매치"
		"MapName"	"dm_deima"
		"LocationX"	"100"
		"LocationY"	"250"
		"ThreatString" "1"    // Threat level string to help players decide where to go next
		"Links"		"dm_desert dm_residential"
		"LocationDescription" "데이마 표면교"
		"ShortBriefing"  "모든 적을 파괴하십시오."
		"AlwaysVisible"	"1"
	}
	"MISSION"
	{
		"MissionName"		"신텍 병원 데스매치"		// name used on the map screen, etc.
		"MapName"		"dm_residential"			// name of the map file
		"LocationX"		"100"				// location of the dot on the map
		"LocationY"		"200"				// (from 0 to 1023, on the above texture)
		"ThreatString" 		"1"    				// Threat level string to help players decide where to go next
		"Links"			"dm_deima dm_testlab"	// map names of neighbours
		"LocationDescription" 	"신텍 병원 데스매치"
		"ShortBriefing"  	"모든 적을 파괴하십시오."
		"AlwaysVisible"		"1"
		"NeedsMoreThanOneMarine" "0"				// If set to 1, two players need to be connected to play (no solo play)
	}
	"MISSION"
	{
		"MissionName"		"실험실 데스매치"		// name used on the map screen, etc.
		"MapName"		"dm_testlab"		// name of the map file
		"LocationX"		"100"				// location of the dot on the map
		"LocationY"		"100"				// (from 0 to 1023, on the above texture)
		"ThreatString" 		"1"    				// Threat level string to help players decide where to go next
		"Links"			"dm_residential"	// map names of neighbours
		"LocationDescription" 	"실험실"
		"ShortBriefing"  	""
		"AlwaysVisible"		"1"
		"NeedsMoreThanOneMarine" "0"				// If set to 1, two players need to be connected to play (no solo play)
	}
}